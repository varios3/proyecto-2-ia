using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuPrincipal : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    { }
    // Update is called once per frame
    void Update()
    {   }
    public void cargar_escena(string escena) {
        GameStatus.insertar_bitacora("[ACCION] Se carga la escena "+escena+" -- " + DateTime.Now.ToString("hh:mm:ss"));
        GameStatus.EscribirBitacora();
        SceneManager.LoadScene(escena);
    }

}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class crear_espacio : MonoBehaviour
{
    // Start is called before the first frame update
    public List<string> lista_pisos;
    Dictionary<string, string> muebles = new Dictionary<string, string>();
    public static LinkedList<string> posValidacion3 = new LinkedList<string>();

    public int contador=0;
    public Dropdown drop;

    //objetipo tipo espacio

    public class espacio { 
        public string imagen {
            get;
            set;
        }
        public string silla {
            get;
            set;
        }
        public string mesa
        {
            get;
            set;
        }
        public string sofa
        {
            get;
            set;
        }
        public string lampara
        {
            get;
            set;
        }
        public string banco
        {
            get;
            set;
        }

    }

    public espacio esp = new espacio();
    //cuadro de alertas
       void Start()
    {
        
    }

    public void piso_valuechange(Dropdown sender) {

        switch (sender.value)
        {
            case 1:
               
                GameStatus.piso = "madera";
                esp.imagen = GameStatus.piso;
                break;
            case 2:

                GameStatus.piso = "grama";
                esp.imagen = GameStatus.piso;
                break;
            case 3:
  
                GameStatus.piso = "concreto";
                esp.imagen = GameStatus.piso;
                break;
            case 4:

                GameStatus.piso = "alfombra";
                esp.imagen = GameStatus.piso;
                break;
            case 5:

                GameStatus.piso = "ceramico";
                esp.imagen = GameStatus.piso;
                break;
            case 6:

                GameStatus.piso = "navidad";
                esp.imagen = GameStatus.piso;
                break;
            default:
                Debug.Log("error");
                break;
        }
    }


    public void mueble_posicion_change(Dropdown sender) {
        if (sender.name == "drop2") {
            if (Ingresar_muebles_a_espacio(sender.options[sender.value].text, "silla"))
            {
                esp.silla = sender.options[sender.value].text;
            }
            else
            {
                esp.silla = "";
                sender.value = 0;
            }
        }
        else if (sender.name == "drop3")
        {
            if (Ingresar_muebles_a_espacio(sender.options[sender.value].text, "mesa"))
            {
                esp.mesa = sender.options[sender.value].text;
            }
            else
            {
                esp.mesa = "";
                sender.value = 0;
            }
        }
        else if (sender.name == "drop4")
        {
            if (Ingresar_muebles_a_espacio(sender.options[sender.value].text, "sofa"))
            {
                esp.sofa = sender.options[sender.value].text;
            }
            else
            {
                esp.sofa = "";
                sender.value = 0;
            }
        }
        else if (sender.name == "drop5")
        {
            if (Ingresar_muebles_a_espacio(sender.options[sender.value].text, "lampara"))
            {
                esp.lampara = sender.options[sender.value].text;
            }
            else
            {
                esp.lampara = "";
                sender.value = 0;
            }
        }
        else
         if (sender.name == "drop6")
        {
            if (Ingresar_muebles_a_espacio(sender.options[sender.value].text, "banco"))
            {
                esp.banco = sender.options[sender.value].text;
            }else{
                esp.banco = "";
                sender.value = 0;
            }

        }
    }
    //metodo que ingresa los muebles y donde vamos aver que no se repitan los muebles en el espacio
    public bool Ingresar_muebles_a_espacio(string posicion, string mueble) {
        if (!posValidacion3.Contains(posicion)) {
            posValidacion3.AddFirst(posicion);
        }

        if (posicion == "" && muebles.ContainsValue(mueble))
        {
            GameStatus.insertar_bitacora("Se quita la posición al mueble " + mueble +  " -- " + DateTime.Now.ToString("hh:mm:ss"));
            var item = (from d in muebles
                        where d.Value == mueble
                        select d.Key).FirstOrDefault();

            GameStatus.insertar_bitacora("item "+item);
            muebles.Remove(item);
            return true;
        }
        else
        {
            if (!muebles.ContainsKey(posicion) && posicion != "")
            {
                GameStatus.insertar_bitacora("Se coloca " + mueble + " en la posición " + " - " + posicion + " " + " -- " + DateTime.Now.ToString("hh:mm:ss"));
                muebles.Add(posicion, mueble);
                return true;
            }
            else
            {
                if (EditorUtility.DisplayDialog("ERROR 0002", "\n Ya existe este mueble("+mueble+") en otra posicion!!!!", "Ok"))
                {
                    GameStatus.insertar_bitacora("[ERROR] 0002 Ya existe este mueble("+mueble+") en otra posicion!!!!" + " -- " + DateTime.Now.ToString("hh:mm:ss"));
                    return false;
                }
                return false;
            }
        }

    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void regresar() {
        SceneManager.LoadScene("MainMenu");
    }
    //inicializar de espacios
    
    public void mandardatos() {
        if (posValidacion3.Count < 5)
        {
            if (EditorUtility.DisplayDialog("ERROR", "El espacio no puede tener posiciones VACIAS", "Intentar de nuevo", "Menu principal"))
            {
                GameStatus.insertar_bitacora("[ERROR]  El espacio no puede tener posiciones VACIAS" + " -- " + DateTime.Now.ToString("hh:mm:ss"));
                SceneManager.LoadScene("crear_espacio");
            }
        }else{
            GameStatus.insertar_lista("imagen_" + GameStatus.contador_espacio.ToString(), GameStatus.piso);
            GameStatus.contador_espacio++;
            GameStatus.insertar_lista_espacios(esp);

            //No se podr� crear m�s de 6 espacios debido a la limitaci�n de los pisos
            //metodo dentro del gamestatus para ver si hay mas de 6 espacios si lo hay-- tira error

            //aqui tambien ingresamos los espacios a la lista global de espacios
            if (GameStatus.Insertar_lista_imagen_pos_muebles(esp.imagen, muebles))
            {

                if (EditorUtility.DisplayDialog("ESCENARIO GENERADO!!", "Escenario Creado!!\n Desea crea otro escenario ó regresar al menu principal?", "Crear otro", "Menu principal"))
                {
                    //aqui agregamos al diccionario el espacio con su imagen
                    GameStatus.insertar_bitacora("[ACCION]Se Creo Espacio " + (GameStatus.contador_espacio - 1).ToString() + " -- " + DateTime.Now.ToString("hh:mm:ss"));
                    GameStatus.EscribirBitacora();
                    SceneManager.LoadScene("crear_espacio");
                }
                else
                {
                    SceneManager.LoadScene("MainMenu");
                }
            }
        }

       
    }
}

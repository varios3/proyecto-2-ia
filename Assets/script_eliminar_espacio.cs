using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class script_eliminar_espacio : MonoBehaviour
{
    public UnityEngine.UI.ToggleGroup ToggleGroup;
    Texture2D myTexture;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var entry in GameStatus.lista_espacios_imagen)
        {
            Debug.Log(entry.Key + ":" + entry.Value);
            myTexture = Resources.Load(entry.Value) as Texture2D;
            GameObject rawImage = GameObject.Find(entry.Key);
            rawImage.GetComponent<RawImage>().texture = myTexture;
        }



    }
    public void LogSelectedToggle()
    {
        // May have several selected toggles

        // OR

        Toggle selectedToggle = ToggleGroup.ActiveToggles().FirstOrDefault();
        if (selectedToggle != null)
        {
            if (selectedToggle.isOn && selectedToggle.name == "espacio_1")
            {
                GameStatus.espacio_seleccionado = 0;

            }
            else if (selectedToggle.isOn && selectedToggle.name == "espacio_2")
            {

                GameStatus.espacio_seleccionado = 1;
            }
            else if (selectedToggle.isOn && selectedToggle.name == "espacio_3")
            {

                GameStatus.espacio_seleccionado = 2;
            }
            else if (selectedToggle.isOn && selectedToggle.name == "espacio_4")
            {

                GameStatus.espacio_seleccionado = 3;
            }
            else if (selectedToggle.isOn && selectedToggle.name == "espacio_5")
            {

                GameStatus.espacio_seleccionado = 4;

            }
            else if (selectedToggle.isOn && selectedToggle.name == "espacio_6")
            {

                GameStatus.espacio_seleccionado = 5;
            }

        }
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void eliminar_espacio()
    {

        try{
            Toggle selectedToggle = ToggleGroup.ActiveToggles().FirstOrDefault();
            int numberSpace = -1;
            if (selectedToggle.isOn && selectedToggle.name == "espacio_1"){
                numberSpace = 0;
            }else if (selectedToggle.isOn && selectedToggle.name == "espacio_2"){
                numberSpace = 1;
            }else if (selectedToggle.isOn && selectedToggle.name == "espacio_3"){
                numberSpace = 2;
            }else if (selectedToggle.isOn && selectedToggle.name == "espacio_4"){
                numberSpace = 3;
            }else if (selectedToggle.isOn && selectedToggle.name == "espacio_5"){
                numberSpace = 4;
            }else if (selectedToggle.isOn && selectedToggle.name == "espacio_6"){
                numberSpace = 5;
            }

            GameStatus.insertar_bitacora("[ACCION]Se Ingresa a Eliminar Escenario " + GameStatus.lista_imagen_sillas[numberSpace].imagen + " -- " + DateTime.Now.ToString("hh:mm:ss"));
            if(GameStatus.lista_imagen_sillas[numberSpace].imagen!=""){
                if (eliminado_mensaje())
                {
                    GameStatus.insertar_bitacora("[ACCION] Se ingresa a Eliminar Espacio -- " + DateTime.Now.ToString("hh:mm:ss"));
                    GameStatus.insertar_bitacora("[ACCION] Se elimino ESPACIO "+(numberSpace+1).ToString()+" -- " + DateTime.Now.ToString("hh:mm:ss"));                   
                    GameStatus.EscribirBitacora();
                    GameStatus.lista_piso_muebles.Remove(GameStatus.lista_imagen_sillas[numberSpace].imagen);
                    GameStatus.lista_espacios_imagen.Remove("imagen_"+(numberSpace+1).ToString());  
                    GameStatus.lista_imagen_sillas.RemoveAt(numberSpace);                
                    SceneManager.LoadScene("eliminar_espacio");
                }
            }
        }catch{
            EditorUtility.DisplayDialog("ERROR ESCENARIOS", "\n No existe escenario para la posición seleccionada!", "Ok");
            GameStatus.insertar_bitacora("[ERROR]No existe escenario para la posición seleccionada!" + " -- " + DateTime.Now.ToString("hh:mm:ss"));
        }
    }
    public bool eliminado_mensaje() {
        if (EditorUtility.DisplayDialog("ELIMINAR ESCENARIO", "\n¿Desea eliminar el Escenario?", "Si", "No"))
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    public void regresar()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
